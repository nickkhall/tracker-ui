// MUI Components
import EventIcon from '@mui/icons-material/Event';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import PhotoCameraBackIcon from '@mui/icons-material/PhotoCameraBack';

// Constants
import { routePaths } from 'static/routePaths';

const { 
  calendar: { path: calendarPath },
  restaurants: { path: restaurantsPath },
  dateIdeas: { path: dateIdeasPath },
  photos: { path: photosPath }
} = routePaths;

export const navItems = [
  {
    label: 'Calendar', 
    icon: EventIcon,
    path: calendarPath
  },
  {
    label: 'Restaurants',
    icon: FastfoodIcon,
    path: restaurantsPath
  },
  {
    label: 'Date Ideas', 
    icon: FavoriteBorderIcon,
    path: dateIdeasPath
  },
  {
    label: 'Photos',
    icon: PhotoCameraBackIcon,
    path: photosPath
  }
];

