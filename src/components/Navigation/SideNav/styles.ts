import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export default makeStyles(({ primary, secondary, text }: Theme) => ({
  label: {
    color: text.light,
    fontSize: '0.8rem !important'
  },
  sideNav: {
    background: primary.main,
    borderRadius: 3,
    textAlign: 'center',
    minWidth: '10vw',
    maxWidth: '300px',
    height: '100%'
  },
  nav: {
    padding: 0
  },
  navItem: {
    textDecoration: 'none'
  },
  navItemContent: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    '&:hover': {
      background: primary.light,
      cursor: 'pointer'
    }
  },
  icon: {
    color: text.light,
    transform: 'scale(1.25)',
    border: `2px solid ${text.light}`,
    borderRadius: '100%',
    margin: '20px 0',
    padding: 5,
    '&:hover': {
      cursor: 'pointer'
    }
  }
}));

