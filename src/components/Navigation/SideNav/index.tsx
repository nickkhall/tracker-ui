import React from 'react';

// MUI
import { SvgIconComponent } from "@mui/icons-material";
import Typography from '@mui/material/Typography';

// Styles
import useStyles from './styles';

// Constants
import { navItems } from './constants';

interface Props {}

interface NavItemsProps {
  label: string;
  icon: SvgIconComponent;
  path: string;
}

export default function SideNav(props: Props) {
  const classes = useStyles();

  return (
    <div className={classes.sideNav}>
      <nav className={classes.nav}>
        {
          navItems.map(({ label, path, icon: Icon }: NavItemsProps) => (
            <a className={classes.navItem} title={label} href={path}>
              <div className={classes.navItemContent}>
                <Icon className={classes.icon} />
                <Typography className={classes.label} variant="body1">{label}</Typography>
              </div>
            </a>
          ))
        }
      </nav>
    </div>
  );
}

