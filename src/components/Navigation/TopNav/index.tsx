import React from 'react';

// MUI Components
import Typography from '@mui/material/Typography';

// Constants
import { navItems } from './constants';

// Styles
import useStyles from './styles';

interface Props {}

export default function TopNav(props: Props) {
  const classes = useStyles();

  return (
    <div className={classes.topNav}>
      <Typography className={classes.title} variant="caption">Tracker</Typography>
      <nav className={classes.nav}>
        {
          navItems.map(({ path, label }) => (
            <a
              className={classes.navItem}
              key={label}
              href={path}
              title={label}
            >
              {label}
            </a>
          ))
        }
      </nav>
    </div>
  );
}

