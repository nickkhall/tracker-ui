import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export default makeStyles(({ primary, text }: Theme) => ({
  title: {
    color: text.light,
    fontSize: '2rem !important'
  },
  topNav: {
    alignItems: 'center',
    background: primary.main,
    borderBottom: `1px solid ${text.light}`,
    display: 'flex',
    justifyContent: 'space-between',
    padding: 20
  },
  nav: {
  },
  navItem: {
    color: text.light,
    margin: '0 10px',
    textDecoration: 'none'
  }
}));

