import React from 'react';

// React Router
import { BrowserRouter, Switch } from 'react-router-dom';

// Components
import Route from 'components/Route';

// Contants
import { routes } from 'static/routes';

interface RouterProps {}

export default function Router(props: RouterProps) {
  return (
    <BrowserRouter>
      <Switch>
        { 
          routes.map(route => (
            <Route key={route.path} {...route} />
          ))
        }
      </Switch>  
    </BrowserRouter>
  );
}
