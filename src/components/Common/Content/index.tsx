import React from 'react';

// MUI Components
import Paper from '@mui/material/Paper';

// Styles
import useStyles from './styles';

interface ContentProps {
  children: React.ReactNode[] | React.ReactNode
}

export default function Content({ children }: ContentProps) {
  const classes = useStyles();

  return (
    <Paper
      className={classes.paper}
      elevation={3}
    >
      { children }
    </Paper>
  );
}

