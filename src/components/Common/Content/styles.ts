import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export default makeStyles(({ secondary }: Theme) => ({
  paper: {
    background: `${secondary.main} !important`,
    display: 'inline-block',
    margin: 20,
    padding: 20,
    height: '100%'
  }
}));

