import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export default makeStyles(({ primary, secondary, text }: Theme) => ({
  modal: {
    background: primary.dark
  },
  cancelBtn: {
    background: secondary.dark,
    color: text.light
  },
  saveBtn: {
    background: primary.light,
    color: text.light
  },
}));

