import React, { useState } from 'react';

// MUI Components
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

// Styles
import useStyles from './styles';

interface ModalProps {
  children: JSX.Element[] | JSX.Element | React.ReactNode[] | React.ReactNode;
  isOpen: boolean;
  title: string;
  btnText?: string;
  handleCancel: () => void;
  handleSave: (data: Object) => void;
}

export default function Modal({
  children,
  isOpen,
  title,
  btnText,
  handleCancel,
  handleSave
}: ModalProps) {
  const classes = useStyles();
  const [formData, setFormData] = useState<Object>({});

  function onSaveClick() {
    handleSave(formData);
    setFormData({});
  }

  return (
    <Dialog
      className={classes.modal}
      open={isOpen}
    >
      <DialogContent>
        <DialogContentText>
          { title }
        </DialogContentText>
        <DialogActions>
          <Button
            className={classes.cancelBtn}
            onClick={handleCancel}
          >
            Cancel
          </Button>
          <Button
            className={classes.saveBtn}
            onClick={onSaveClick}
          >
            {(btnText || 'Save')}
          </Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  );
}

