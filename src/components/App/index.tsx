import React from 'react';

// Components
import Main from 'components/Main';
import Router from 'components/Router';
import TopNav from 'components/Navigation/TopNav';
import SideNav from 'components/Navigation/SideNav';

// Styles
import useStyles from './styles';

function App() {
  const classes = useStyles();

  return (
    <Main>
      <TopNav />
      <div className={classes.content}>
        <SideNav />
        <div className={classes.mainContent}>
          <Router />
        </div>
      </div>
    </Main>
  );
}

export default App;
