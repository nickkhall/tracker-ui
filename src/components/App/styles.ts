import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export default makeStyles(({ secondary }: Theme) => ({
  content: {
    display: 'flex',
    height: '100%'
  },
  mainContent: {
    display: 'flex',
    height: '100%',
    overflow: 'auto'
  }
}));

