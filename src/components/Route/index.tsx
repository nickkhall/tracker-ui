import React from 'react';

// React Router
import { Route as ReactRoute } from 'react-router-dom';

interface RouteProps {
  path: string;
  component: React.FunctionComponent<JSX.Element | React.ReactNode>;
}

interface ContentProps {
  component: React.FunctionComponent<JSX.Element | React.ReactNode>;
}

function Content({ component: Component }: ContentProps): JSX.Element {
  return <Component />;
}

export default function Route(props: RouteProps): JSX.Element {
  console.log({ currentRoutePath: props.path });
  return (
    <ReactRoute path={props.path}>
      <Content
        component={props.component}
      />
    </ReactRoute>
  );
}
