export const months = [
  {
    name: 'January',
    nickName: 'Jan'
  },
  {
    name: 'February',
    nickName: 'Feb'
  },
  {
    name: 'March',
    nickName: 'Mar'
  },
  {
    name: 'April',
    nickName: 'Apr'
  },
  {
    name: 'May',
    nickName: 'May'
  },
  {
    name: 'June',
    nickName: 'Jun'
  },
  {
    name: 'July',
    nickName: 'Jul'
  },
  {
    name: 'August',
    nickName: 'Aug'
  },
  {
    name: 'September',
    nickName: 'Sep'
  },
  {
    name: 'October',
    nickName: 'Oct'
  },
  {
    name: 'November',
    nickName: 'Nov'
  },
  {
    name: 'December',
    nickName: 'Dec'
  }
];

export const days = [
  {
    day: 1
  },
  {
    day: 2
  },
  {
    day: 3
  },
  {
    day: 4
  },
  {
    day: 5
  },
  {
    day: 6
  },
  {
    day: 7
  },
  {
    day: 8
  },
  {
    day: 9
  },
  {
    day: 10
  },
  {
    day: 11
  },
  {
    day: 12
  },
  {
    day: 13
  },
  {
    day: 14
  },
  {
    day: 15
  },
  {
    day: 16
  },
  {
    day: 17
  },
  {
    day: 18
  },
  {
    day: 19
  },
  {
    day: 20
  },
  {
    day: 21
  },
  {
    day: 22
  },
  {
    day: 23
  },
  {
    day: 24
  },
  {
    day: 25
  },
  {
    day: 26
  },
  {
    day: 27
  },
  {
    day: 28
  },
  {
    day: 29
  },
  {
    day: 30
  },
  {
    day: 31
  },
];

export interface EventProps {}

export interface Event {
  id:          string;
  time:        number;
  title:       string;
  description: string;
  day:         number;
  month:       number;
  year:        number;
}
