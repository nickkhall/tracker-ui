import React, { useState } from 'react';

// MUI Components
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';

// Components
import Content from 'components/Common/Content';
import Modal from 'components/Common/Modal';

// Constants
import { months, days } from './constants';

// Styles
import useStyles from './styles';

interface EventsProps {}

export default function Events(props: EventsProps) { 
  const classes = useStyles();
  const [newEvent, setNewEvent] = useState<number | null>(null);

  function handleClick(day: number) {
    setNewEvent(day);
  }

  function createEvent(data: Object) {
    console.log({ data });
  }

  const newDate = new Date;

  const currentDay = newDate.getDate();
  const currentMonth = newDate.getMonth();
  const currentYear = newDate.getFullYear();

  return (
    <>
      <Content>
        <aside className={classes.calendarInfo}>
          <Typography className={classes.title} variant="h3">Events</Typography>
          <div className={classes.monthInfo}>
            <Typography className={classes.month} variant="h3">
              {months[currentMonth].name}
            </Typography>
            <Typography className={classes.year} variant="h2">
              {currentYear}
            </Typography>
          </div>
        </aside>
        <Divider className={classes.divider} />
        <div className={classes.calendar}>
          {
            days.map(({ day }) => {
              const calendarItemClassName = (currentDay === day)
                ? classes.currentDay
                : classes.day;

              return (
                <Card
                  className={calendarItemClassName}
                  key={day}
                  onClick={() => handleClick(day)}
                  title="Click to add event"
                >
                  {day}
                </Card>
              );
            })
          }
        </div>
        <CardContent>
          
        </CardContent>
      </Content>
      <Modal
        isOpen={!!newEvent}
        title="Create event"
        btnText="Create"
        handleCancel={() => setNewEvent(null)}
        handleSave={createEvent}
      >
        are you sure?
      </Modal>
    </>
  );
}
