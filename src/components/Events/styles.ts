import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export default makeStyles(({ primary, secondary, text }: Theme) => ({
  title: { color: text.light },
  month: {
    color: primary.light,
    marginRight: '10px !important'
  },
  monthInfo: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'flex-end',
    width: '50%'
  },
  year: { color: primary.light },
  divider: {
    background: text.light,
    margin: '10px 0 !important'
  },
  calendar: {
    alignItems: 'center',
    display: 'flex',
    flexWrap: 'wrap'
  },
  calendarInfo: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between'
  },
  currentDay: {
    background: `${primary.light} !important`,
    display: 'flex',
    flexDirection: 'column',
    margin: 10,
    padding: 20,
    height: 75,
    width: 100,
    '&:hover': {
      cursor: 'pointer'
    }
  },
  day: {
    color: text.light,
    display: 'flex',
    flexDirection: 'column',
    margin: 10,
    padding: 20,
    height: 75,
    width: 100,
    '&:hover': {
      background: primary.light,
      cursor: 'pointer'
    }
  }
}));

