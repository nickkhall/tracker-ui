import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

export default makeStyles(({ primary }: Theme) => ({
  main: {
    background: primary.dark,
    display: 'flex',
    flexDirection: 'column',
    height: '100%'
  }
}));

