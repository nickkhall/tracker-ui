import React from 'react';

// Styles
import useStyles from './styles';

interface MainProps {
  children: React.ReactNode[] | React.ReactNode
}

export default function Main({ children }: MainProps) {
  const classes = useStyles();

  return (
    <main className={classes.main}>
      { children }
    </main>
  );
}
