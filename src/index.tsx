import React from 'react';
import ReactDOM from 'react-dom';

// MUI Components
import { ThemeProvider } from '@mui/material/styles';

// MUI Theme
import { theme } from 'static/theme';

// Components
import App from 'components/App';

// Styles
import 'styles/index.css';

// Utils
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
