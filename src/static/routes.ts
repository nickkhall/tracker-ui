// Components
import Home from 'components/Home';
import Dashboard from 'components/Dashboard';
import Settings from 'components/Settings';
import Calendar from 'components/Calendar';

// Constants
import { routePaths } from './routePaths';

export const routes = [
  {
    label: 'Home',
    path: routePaths.home.path,
    component: Home,
    exact: true
  },
  {
    label: 'Dashboard',
    path: routePaths.dashboard.path,
    component: Dashboard,
    exact: true
  },
  {
    label: 'Settings',
    path: routePaths.settings.path,
    component: Settings,
    exact: true
  },
  // Content Components
  {
    label: 'Calendar',
    path: routePaths.calendar.path,
    component: Calendar,
    exact: true
  }
];

