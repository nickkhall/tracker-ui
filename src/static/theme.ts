import { createTheme } from '@mui/material/styles';

declare module '@mui/material/styles' {
  interface Theme {
    primary: {
      light: string;
      main: string;
      dark: string;
    }
    secondary: {
      light: string;
      main: string;
      dark: string;
    }
    text: {
      light: string;
      main: string;
      dark: string;
    }
  }

  interface ThemeOptions {
    primary: {
      light: string;
      main: string;
      dark: string;
    }
    secondary: {
      light: string;
      main: string;
      dark: string;
    }
    text: {
      light: string;
      main: string;
      dark: string;
    }
  }
}

export const theme = createTheme({
  primary: {
    light: '#8AC0D1',
    main: '#171821',
    dark: '#1b1c21'
  },
  secondary: {
    light: '#888',
    main: '#555',
    dark: '#222'
  },
  text: {
    light: '#FFF',
    main: '#FAFAFA',
    dark: '#000'
  }
});

