export const routePaths = {
  home: {
    path: '/'
  },
  dashboard: {
    path: '/dashboard'
  },
  settings: {
    path: '/settings'
  },
  calendar: {
    path: '/calendar'
  },
  restaurants: {
    path: '/restaurants'
  },
  dateIdeas: {
    path: '/dateideas'
  },
  photos: {
    path: '/photos'
  }
};
