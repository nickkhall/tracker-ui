interface Request {
  url:    string;
  method: string;
  body?:   (Object | null | undefined | string)
}

/**
 * Decorates the headers for a request.
 * @param {string} token The jwt token.
 * @return {!Object} The decorated headers object.
 */
const decorateHeaders = (token: string): Object => {
  console.log({ token });
  const headers = {
    // Authorization: `Bearer ${token}`,
    'Content-Type': 'application/json'
  };

  return headers;
};

/**
 * Request wrapper for all request types (GET, POST, PUT, DELETE, etc).
 * @param {string} url The URL for the request.
 * @param {string} method The method (GET, POST, etc) for the request.
 * @param {!Object} body Optional request body.
 * @return {!Promise} Promise containing the axios response.
 */
export const makeRequest = async (req: Request) => {
  const { url, method = 'GET', body } = req;
  try {
    const headers = decorateHeaders('blah');
    const dataPayload = body
      ? { body: JSON.stringify(body) }
      : {};

    const res = await fetch(
      url,
      {
        method,
        headers: { 'Content-Type': 'application/json' },
        ...dataPayload 
      }
    );

    if (res && res.status >= 200 && res.status < 400) return res;

    // If we made it this far, then we have a bad request, or an invalid JWT.
    const { message, problems } = await res.json();
    if (message && !!message.match(/invalid\s?jwt/gi)) {
      console.error('invalid token');
    }

    // throw error to reject Promise
    throw new Error('invalid token');
  } catch (err) {
    return Promise.reject(err);
  }
};
