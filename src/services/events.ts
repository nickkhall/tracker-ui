// Constants
import { config } from './config';

// Utils
import { makeRequest } from './index';

export const createEvent = (event: Object): (Promise<Object> | null) => {
  if (!event) return null;
  const { host, port } = config;

  return makeRequest({
    url: `${host}:${port}/events`,
    method: 'POST',
    body: event
  });
}
